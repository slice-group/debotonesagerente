Rails.application.routes.draw do
  mount InyxBlogRails::Engine, :at => '', as: 'post'
  mount InyxContactUsRails::Engine, :at => '', as: 'messages' 
  mount InyxCatalogRails::Engine, :at => '', as: 'catalog'
  root to: 'frontend#index'

  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  scope :admin do
  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
