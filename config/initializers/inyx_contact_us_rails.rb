# Agregar datos de configuración
InyxContactUsRails.setup do |config|
	config.mailer_to = "info@debotonesagerente.com"
	config.mailer_from = "info@debotonesagerente.com"
	config.name_web = "De botones a Gerente"
	# Activar o Desactivar la ruta especificada ("messages/new")
	config.messages_new = false
	#Route redirection after send
	config.redirection = "/#contact_us"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LfaRwUTAAAAAOzam9LxaqRBxRCXvBl_0sZhC5te"
	  config.private_key = "6LfaRwUTAAAAABFuDQGDRstn3o00W8_vdnk6UJOA"
	end
end