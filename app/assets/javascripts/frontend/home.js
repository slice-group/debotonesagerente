$(document).on('ready page:load', function(arguments){

	function timedCount(i) {
		if (i > 3) { i = 1; }
		actual = i;
		if (i == 3) { next = 1 } else { next = i+1; }
		$("#div-"+actual).removeClass('active');
		$("#div-"+next).addClass('active');
		setTimeout(function(){ i++; timedCount(i); }, 10000);
	}

	timedCount(1);
});