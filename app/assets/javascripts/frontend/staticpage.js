function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

$( document ).ready(function() {

	$('.top-nav').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 1500,
    scrollThreshold: 0.5,
    filter: ':not(.external)',
    easing: 'swing',
    begin: function() {
      removeHash();

    },
    end: function() {
      $('#front-navbar-2').removeClass('in');

      

    },
    scrollChange: function($currentListItem) {

      var num = parseInt($('#home').css('height').split("px")[0]);

        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
              $('#front-navbar').addClass('navbar-fixed-top');
            } else {
                $('#front-navbar').removeClass('navbar-fixed-top');
            }
        });
    }

  });
});









