var ctrl = {};
(function(){	

	this.itemSelected = function(object, $scope){
		if (!$scope.pagesItemsSelected[$scope.currentPage+1]) $scope.pagesItemsSelected[$scope.currentPage+1] = []
			
		if(object.checked){
			$scope.pagesItemsSelected[$scope.currentPage+1].push(object.id);	
		}else{
			var index = $scope.pagesItemsSelected[$scope.currentPage+1].indexOf(object.id);
			if (index>-1){
				$scope.pagesItemsSelected[$scope.currentPage+1].splice(index, 1);
			}
		}
		$scope.btnDelete = ($scope.pagesItemsSelected[$scope.currentPage+1].length>0) ? true:false;
	}

	this.allItemsSelected= function($scope, data){
		if (!$scope.pagesItemsSelected[$scope.currentPage+1]) $scope.pagesItemsSelected[$scope.currentPage+1] = []	
		var intervalo_a = ($scope.currentPage==0) ? $scope.currentPage : $scope.currentPage*$scope.pageSize;
		var intervalo_b = ($scope.currentPage==0) ? $scope.currentPage+$scope.calculateLengthPage() : (($scope.currentPage+1)*$scope.pageSize)+$scope.calculateLengthPage();
		
		//checked
		for(var i=intervalo_a; i<intervalo_b; i++) {
			$scope.objects[i].checked = true;
			var index = $scope.pagesItemsSelected[$scope.currentPage+1].indexOf($scope.objects[i].id);
			if (index==-1) $scope.pagesItemsSelected[$scope.currentPage+1].push($scope.objects[i].id)
			if (i == ($scope.lengthPage+intervalo_a)-1) break;
		}
		console.log($scope.pagesItemsSelected)
		$scope.btnDelete = true;
	}

	this.allItemRemoveSelected = function($scope){
		if (!$scope.pagesItemsSelected[$scope.currentPage+1]) $scope.pagesItemsSelected[$scope.currentPage+1] = []
		var intervalo_a = ($scope.currentPage==0) ? $scope.currentPage : $scope.currentPage*$scope.pageSize;
		var intervalo_b = ($scope.currentPage==0) ? $scope.currentPage+$scope.calculateLengthPage() : (($scope.currentPage+1)*$scope.pageSize)+$scope.calculateLengthPage();

		//unchecked
		for(var i=intervalo_a; i<intervalo_b; i++) {
			$scope.objects[i].checked = false;
			var index = $scope.pagesItemsSelected[$scope.currentPage+1].indexOf($scope.objects[i].id);
			if (index>-1) $scope.pagesItemsSelected[$scope.currentPage+1].splice(index, 1);
			if (i == ($scope.lengthPage+intervalo_a)-1) break;
		}
		console.log($scope.pagesItemsSelected)
		$scope.btnDelete = false;
	}

	this.updateItemSelected = function($scope) {
		for(var key in $scope.pagesItemsSelected) {
			for(var i in $scope.pagesItemsSelected[key]) {
				for(var object in $scope.objects) {
					if($scope.pagesItemsSelected[key][i] == $scope.objects[object].id) {
						$scope.objects[object].checked = true
						break;
					}
				}
			}
		}
		ctrl.initializaArrayItemsSelect($scope)
		ctrl.updateIdentifyPageAfterDelete($scope)
		ctrl.selectedAfterDelete($scope)
	}

	this.updateIdentifyPageAfterDelete = function($scope) {
		page = 0;
		for(var i=0; i<$scope.objects.length; i++) {
			if (i==(page*$scope.pageSize)) { page++ }
			$scope.objects[i].identifyPage = page
		}
	}
	
	
	this.createArrayItemsSelect = function($scope){
		return $scope.pagesItemsSelected[$scope.currentPageServer];
	}

	this.initializaArrayItemsSelect = function($scope){
		for(var key in $scope.pagesItemsSelected) {
			$scope.pagesItemsSelected[key] = [];
		}
	}


	this.updateBtnDelete = function($scope) {
    if (!$scope.pagesItemsSelected[$scope.currentPage+1]) $scope.pagesItemsSelected[$scope.currentPage+1] = []

    if ($scope.pagesItemsSelected[$scope.currentPage+1].length == 0) {
      $scope.btnDelete = false;
    } else {
      $scope.btnDelete = true;
    }
	}

	//metodo para reubicar la pagina a una existente depsues de eliminar items.
	this.currentPageAfterRemove = function($scope) {
		if($scope.currentPage > $scope.numberOfPages()) {
			$scope.currentPage = $scope.numberOfPages()-1;
		}
	}


	this.selectedAfterLoad = function($scope, selected) {
		for(var select in selected) {
			for(var object in $scope.objects) {
				if(selected[select] == $scope.objects[object].id) {
					$scope.objects[object].checked = true
					break;
				}
			}
		}
	}

	this.selectedAfterDelete = function($scope) {
		for(var object in $scope.objects) {
			if ($scope.objects[object].checked) { 
				$scope.pagesItemsSelected[$scope.objects[object].identifyPage].push($scope.objects[object].id)
			}
		}

	}

	this.updateItemsPerPage = function($scope, data) {
		

		if($scope.isFirstPage()) {
			

			for(var i = $scope.currentPage*$scope.pageSize; i< $scope.currentPage+$scope.lengthPage; i++) {
				$scope.objects[i] = data[i]
				$scope.objects[i].identifyPage = $scope.currentPageServer
			}
			
		} else {

			for(var i = $scope.currentPage*$scope.pageSize; i < ($scope.currentPage*$scope.pageSize)+$scope.lengthPage; i++) {
				$scope.objects[i] = data[i-($scope.currentPage*$scope.pageSize)]
				$scope.objects[i].identifyPage = $scope.currentPageServer
			}
			
			
		}
	}

	this.deleteItemsLastPage = function($scope, selected) {
		for (var select in selected) {
			for (index in $scope.objects) {
				if(selected[select] == $scope.objects[index].id) {
					if ($scope.object_data && selected[select] == $scope.object_data.id) $scope.object_data = null;
					$scope.objects.splice(index, 1);
					break;
				}
			}
		}
	}	

	this.hashErrors = {
		1: [
			"001",
			"Probando los alerts",
			"danger"
		]
	};

}).apply(ctrl);
