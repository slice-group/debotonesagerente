// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require tinymce
//= require angular
//= require angular-route
//= require nprogress
//= require nprogress-angular
//= require nprogress-turbolinks
//= require inyx_contact_us_rails/application
//= require inyx_catalog_rails/application
//= require bootstrap-datepicker
//= require turbolinks
//= require bootstrap
//= require blog/admin/application
//= require_tree .


NProgress.configure({
  showSpinner: true,
  ease: 'ease',
  speed: 500,
  parent: '.main-admin'
});

$(document).on('ready page:load', function(arguments){
	angular.bootstrap(document.body, ['inyxmater']);
});

$(document).on('ready page:load', function(arguments){
	$(document).ready(function(){
		$('.authform').animate({
		    opacity: 1,
		  }, 1000);
	});
});