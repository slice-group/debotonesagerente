class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
  def index
  	Location.add(request.remote_ip)
  	@posts = InyxBlogRails::Post.where(public: true).order("created_at DESC").limit(3)
  	@services = InyxCatalogRails::Catalog.where(public: true)
  end
end
